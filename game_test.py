"""Unit tests for the Game"""
from unittest import TestCase

from game import Game


class BowlingGameTest(TestCase):
    """Edge case testing class"""

    def setUp(self):
        "Executes before each test"
        self.game = Game()

    def test_gutter_game(self):
        "Player rolls 20 zeros"
        self._roll_many(20, 0)
        self.assertEquals(0, self.game.score)

    def test_ones_only(self):
        "Player rolls 20 ones"
        self._roll_many(20, 1)
        self.assertEquals(20, self.game.score)

    def test_a_spare(self):
        "Player rolls a spare"
        self.game.roll(4)
        self.game.roll(6)  # triggers a spare
        self.game.roll(7)
        self.assertEquals(24, self.game.score)

    def _roll_many(self, rolls_number, pins):
        "roll same number of pins a roll_number of times"
        for _ in range(rolls_number):
            self.game.roll(pins)
