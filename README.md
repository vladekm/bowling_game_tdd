===================================
Bowling Game TDD Exercice in Python
===================================

Introduction
============
The following exercise can be used to introduce the basic flow of Test Driven Development. This is based on [The Bowling Game Kata](http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata) by Rober Martin.


Problem description
===================

A bowling game consists of 10 frames comprising of two rolls each. In each frame the player has two opportunities to knock down 10 pins.  The score for the frame is the total number of pins knocked down, plus bonuses for strikes and spares.

* frame1: 0,4
* frame2: 3,7
* frame3: 5,2
* frame4: 10
* frame5: 0,0
* ...

A spare is when the player knocks down all 10 pins in two tries. The bonus for that frame is the number of pins knocked down by the next roll.  So in frame 2 above, the score is 10 (the total number knocked down) plus a bonus of 5 (the number of pins knocked down on the next roll.)

A strike is when the player knocks down all 10 pins on his first try.  The bonus for that frame is the value of the next two balls rolled.
In the tenth frame a player who rolls a spare or strike is allowed to roll the extra balls to complete the frame.  However no more than three balls can be rolled in tenth frame.


Requirements
============

Write a simple system capable of accepting the results of consecutive rolls. The system also calculates and exposes the score.

Interface
---------

'Game' concept exposes the following:

* roll(int) - a roll has been executed and int of pins went down
* score() - score resulting from all the rolls in the game

'Game' also uses 'Store' to persist the results of rolls. 'Store' should be treated as an external dependency. It is provided to emulate remote service. Each call to 'Store' costs 5 seconds.

'Store' exposes the following:

* init_game() - initializes an empty storage for a game and returns a unique id assigned to the storage
* add_roll(game_id, int) - stores the roll result

Methodology
-----------

The code is written using Test Driven Development. 'Store' and its tests are treated as a 'given'.


How to use
==========


In order to start:

1. Checkout this code.
```
git clone git@bitbucket.org:vladekm/bowling_game_tdd.git
```
2. Create a virtualenv and pip install the requirements
```
pip install -r requirements.txt
```
3. Run tests
```
nose
```

This exercise can be used in an interactive pair programming session where the navigator has access to the repository and is guiding the driver towards the solution. Each commit in the directory represents the natural Red/Green/Refactor phases of the TDD.
