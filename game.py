"""Score calculation"""


class Game(object):
    """Class for score calculation"""

    def __init__(self):
        self.results = []

    def roll(self, pins):
        """Register a new roll

        Args:
            pins (int): number of pins
        """
        self.results.append(pins)

    @property
    def score(self):
        "Return the score; works as a property."
        return sum(self.results)
